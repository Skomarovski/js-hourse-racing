let {
  showHorses,
  showAccount,
  setWager,
  startRacing,
  newGame
} = (() => {
  let wagers, 
      player, 
      horsesList, 
      horsesNameList;

  // Конструктор игроков
  function Player(name, account, wagers) {
    this.name = name;
    this.account = account;
    this.wagers = wagers;
    this.getName = () => name;
    this.getAccount = () => this.account;
    this.removeFromAccount = (amount) => this.account -= amount;
    this.addToAccount = (amount) => this.account += amount;
  }

  // Конструктор скакунов
  function Horse(name) {
    this.name = name;
    this.getName = () => this.name;
    this.run = () => {
      return new Promise((resolve) => {
        setTimeout(() => resolve(this), 
        getRandomNumBetween(500, 3000))
      })
    }
  }

  // Конструктор хранилища ставок
  function WagersStorage() {
    this.history = {};
    this.putToHistory = (horseName, wager) => {
      this.history[horseName] = wager
    };
    this.clear = () => this.history = {};
    this.getHistory = () => this.history;
  }

  function getRandomNumBetween(minNum, maxNum) {
    return Math.random() * (maxNum - minNum) + minNum;
  }

  return {
    // Показать список лошадей
    showHorses: () => {
      console.log('Список лошадей:')
      for (let i = 0; i < horsesList.length; i++) {
        console.log(`\t${i+1}. ${horsesNameList[i]}`);
      }
    },
    
    // Показать счет игрока
    showAccount: () => {
      console.log(`На счету игрока ${player.getName()} ${player.getAccount()}$`);
    },

    // Сделать ставку на лошадь в следующем забеге
    setWager: (horseName, wagerAmount) => {
      try {
        if (player.account >= wagerAmount && horsesNameList.includes(horseName)) {
          player.removeFromAccount(wagerAmount);
          player.wagers.putToHistory(horseName, wagerAmount);
        } else if (horseName === undefined || wagerAmount === undefined) {
          throw new Error('Введены не все параметры.');
        } else if (!horsesNameList.includes(horseName)) {
          throw new Error('Не правильно введено имя лошади.');
        } else {
          throw new Error('На вашем счету недостаточно средств для данной ставки.')
        }
      }
      catch(err) {
        console.log(`Ошибка: ${err.message}`);
      }
    },

    // Начать забег
    startRacing: () => {
      let raceResults = [];
      let raceWinnerName = '';

      horsesList.forEach((horse) => {
        const individualResult = horse.run();
        individualResult.then(() => {
          console.log(horse.getName());
        })
        raceResults.push(individualResult);
      })

      Promise.race(raceResults).then((winner) => {
        raceWinnerName = winner.getName();
      });
      Promise.all(raceResults).then(() => {
        const wagersHistory = player.wagers.getHistory();
        const horsesNamesWager = Object.keys(wagersHistory);
        if (horsesNamesWager.includes(raceWinnerName)) {
          let winning = wagersHistory[raceWinnerName] * 2;
          player.account += winning;
        }
        showAccount();
        player.wagers.clear();
      })
    },

    // Начать игру
    newGame: () => {
      // Создание хранилищя ставок
      wagers = new WagersStorage()

      // Создание профиля игрока
      player = new Player('Инкогнито', 100, wagers);

      // Создание лошадей
      horsesList = [new Horse('Хромой Томми'),
                      new Horse('Лучик'),
                      new Horse('Диди Мегадуду'),
                      new Horse('Смельчак')];
      horsesNameList = horsesList.map((horse) => horse.getName());
    },
  };
})();

newGame();